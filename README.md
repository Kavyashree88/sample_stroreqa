# Sample_StroreQA

**Prerequisite|Java**

1. Java is installed on your system.
2. IDE is available
3. Maven Java Project is created in IDE

**Prerequisite|TestNG**

1. TestNG plugin is added in IDE.
2. TestNG dependency is added in pom.xml
3. TestNG tests are created.
4. testng.xml file is created.

**Prerequisite|Maven**

1. Maven should be available on your system.

**How to run testng from maven command line**

*Steps:*
1. Add maven surefire plugin in pom.xml.
2. Provide location of your testng.xml file.
3. Open command and goto location of project.
4. Run command | mvn clean test.


